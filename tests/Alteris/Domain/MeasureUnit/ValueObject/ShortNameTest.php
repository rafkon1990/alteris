<?php

namespace App\Tests\Alteris\Domain\MeasureUnit\ValueObject;

use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMinException;
use App\Alteris\Domain\MeasureUnit\ValueObject\ShortName;
use PHPUnit\Framework\TestCase;

class ShortNameTest extends TestCase
{
    /**
     * @throws ShortNameMinException
     * @throws ShortNameMaxException
     */
    public function testMin(): void
    {
        $this->expectException(ShortNameMinException::class);
        new ShortName('');
    }

    /**
     * @throws ShortNameMinException
     * @throws ShortNameMaxException
     */
    public function testMax(): void
    {
        $this->expectException(ShortNameMaxException::class);
        new ShortName(str_repeat('a', 256));
    }

    /**
     * @throws ShortNameMinException
     * @throws ShortNameMaxException
     */
    public function testValidMin(): void
    {
        $name = new ShortName(str_repeat('a', 3));
        $this->assertEquals('aaa', $name->getValue());
    }

    /**
     * @throws ShortNameMinException
     * @throws ShortNameMaxException
     */
    public function testValidMax(): void
    {
        $name = new ShortName(str_repeat('a', 5));
        $this->assertEquals('aaaaa', $name->getValue());
    }
}
