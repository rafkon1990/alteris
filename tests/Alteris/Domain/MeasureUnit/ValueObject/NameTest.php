<?php

namespace App\Tests\Alteris\Domain\MeasureUnit\ValueObject;

use App\Alteris\Domain\MeasureUnit\Exception\NameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\NameMinException;
use App\Alteris\Domain\MeasureUnit\ValueObject\Name;
use PHPUnit\Framework\TestCase;

class NameTest extends TestCase
{
    /**
     * @throws NameMinException
     * @throws NameMaxException
     */
    public function testMin(): void
    {
        $this->expectException(NameMinException::class);
        new Name(str_repeat('a', 2));
    }

    /**
     * @throws NameMinException
     * @throws NameMaxException
     */
    public function testMax(): void
    {
        $this->expectException(NameMaxException::class);
        new Name(str_repeat('a', 256));
    }

    /**
     * @throws NameMinException
     * @throws NameMaxException
     */
    public function testValidMin(): void
    {
        $name = new Name(str_repeat('a', 3));
        $this->assertEquals('aaa', $name->getValue());
    }

    /**
     * @throws NameMinException
     * @throws NameMaxException
     */
    public function testValidMax(): void
    {
        $name = new Name(str_repeat('a', 255));
        $this->assertEquals(str_repeat('a', 255), $name->getValue());
    }
}
