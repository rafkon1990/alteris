<?php

namespace App\Tests\Alteris\Domain\MeasureUnit\CommandHandler;

use App\Alteris\Domain\MeasureUnit\Command\CreateMeasureUnit;
use App\Alteris\Domain\MeasureUnit\CommandHandler\CreateMeasureUnitHandler;
use App\Alteris\Domain\MeasureUnit\Exception\NameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\NameMinException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMinException;
use App\Alteris\Domain\MeasureUnit\Exception\IdNotUniqueException;
use App\Alteris\Domain\MeasureUnit\Factory\MeasureUnitFactory;
use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit;
use App\Alteris\Domain\MeasureUnit\Repository\MeasureUnitRepositoryInterface;
use App\Alteris\Domain\MeasureUnit\ValueObject\Name;
use App\Alteris\Domain\MeasureUnit\ValueObject\ShortName;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use App\Alteris\Infrastructure\Persistence\InMemory\MeasureUnitRepository;
use PHPUnit\Framework\TestCase;

class CreateMeasureUnitHandlerTest extends TestCase
{
    /** @var MeasureUnitRepositoryInterface */
    private $repository;

    /** @var MeasureUnitFactory */
    private $factory;

    protected function setUp(): void
    {
        $this->repository = new MeasureUnitRepository();
        $this->factory = new MeasureUnitFactory();
    }

    /**
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ShortNameMaxException
     * @throws ShortNameMinException
     * @throws IdNotUniqueException
     */
    public function testUuidNotUnique(): void
    {
        $uuidValue = 'a5522e67-e384-491d-8c4a-bed62a7246a1';
        $this->repository->save(new MeasureUnit(new Uuid($uuidValue), new Name('test'), new ShortName('t')));
        $handler = new CreateMeasureUnitHandler($this->repository, $this->factory);
        $command = new CreateMeasureUnit($uuidValue, 'meter', 'm');
        $this->expectException(IdNotUniqueException::class);
        $handler($command);
    }

    /**
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ShortNameMaxException
     * @throws ShortNameMinException
     * @throws IdNotUniqueException
     */
    public function testValid(): void
    {
        $handler = new CreateMeasureUnitHandler($this->repository, $this->factory);
        $command = new CreateMeasureUnit('a5522e67-e384-491d-8c4a-bed62a7246a1', 'meter', 'm');
        $handler($command);
        $this->assertNotNull($this->repository->findById('a5522e67-e384-491d-8c4a-bed62a7246a1'));
    }
}
