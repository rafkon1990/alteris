<?php

namespace App\Tests\Alteris\Domain\MeasureUnit\CommandHandler;

use App\Alteris\Domain\MeasureUnit\Command\UpdateMeasureUnit;
use App\Alteris\Domain\MeasureUnit\CommandHandler\UpdateMeasureUnitHandler;
use App\Alteris\Domain\MeasureUnit\Exception\MeasureUnitNotFoundException;
use App\Alteris\Domain\MeasureUnit\Exception\NameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\NameMinException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMinException;
use App\Alteris\Domain\MeasureUnit\Exception\IdNotUniqueException;
use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit;
use App\Alteris\Domain\MeasureUnit\Repository\MeasureUnitRepositoryInterface;
use App\Alteris\Domain\MeasureUnit\ValueObject\Name;
use App\Alteris\Domain\MeasureUnit\ValueObject\ShortName;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use App\Alteris\Infrastructure\Persistence\InMemory\MeasureUnitRepository;
use PHPUnit\Framework\TestCase;

class UpdateMeasureUnitHandlerTest extends TestCase
{
    /** @var MeasureUnitRepositoryInterface */
    private $repository;

    /** @var string */
    private $existingUuidValue = '6bc49d57-e865-4e19-b06c-955f2c0d0ca8';

    /** @var string */
    private $nonExistingUuidValue = 'd59c1240-b2f9-4518-aa16-efd1a7a65dcb';

    protected function setUp(): void
    {
        $this->repository = new MeasureUnitRepository(
            new MeasureUnit(new Uuid($this->existingUuidValue), new Name('time'), new ShortName('s'))
        );
    }

    /**
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ShortNameMaxException
     * @throws ShortNameMinException
     * @throws MeasureUnitNotFoundException
     */
    public function testUpdateNotExisting(): void
    {
        $handler = new UpdateMeasureUnitHandler($this->repository);
        $command = new UpdateMeasureUnit($this->nonExistingUuidValue, 'kelvin', 'K');
        $this->expectException(MeasureUnitNotFoundException::class);
        $handler($command);
    }

    /**
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ShortNameMaxException
     * @throws ShortNameMinException
     * @throws MeasureUnitNotFoundException
     */
    public function testValid(): void
    {
        $handler = new UpdateMeasureUnitHandler($this->repository);
        $command = new UpdateMeasureUnit($this->existingUuidValue, 'kelvin', 'K');
        $handler($command);
        $model = $this->repository->findById($this->existingUuidValue);
        $this->assertNotNull($model);
        $this->assertEquals('kelvin', $model->getName());
        $this->assertEquals('K', $model->getShortName());
    }
}
