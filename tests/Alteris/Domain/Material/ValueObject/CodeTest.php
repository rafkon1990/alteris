<?php

namespace App\Tests\Alteris\Domain\Material\ValueObject;

use App\Alteris\Domain\Material\Exception\CodeMaxException;
use App\Alteris\Domain\Material\Exception\CodeMinException;
use App\Alteris\Domain\Material\ValueObject\Code;
use PHPUnit\Framework\TestCase;

class CodeTest extends TestCase
{
    /**
     * @throws CodeMinException
     * @throws CodeMaxException
     */
    public function testMin(): void
    {
        $this->expectException(CodeMinException::class);
        new Code('');
    }

    /**
     * @throws CodeMinException
     * @throws CodeMaxException
     */
    public function testMax(): void
    {
        $this->expectException(CodeMaxException::class);
        new Code(str_repeat('a', 33));
    }

    /**
     * @throws CodeMinException
     * @throws CodeMaxException
     */
    public function testValidMin(): void
    {
        $name = new Code(str_repeat('a', 3));
        $this->assertEquals('aaa', $name->getValue());
    }

    /**
     * @throws CodeMinException
     * @throws CodeMaxException
     */
    public function testValidMax(): void
    {
        $name = new Code(str_repeat('a', 32));
        $this->assertEquals(str_repeat('a', 32), $name->getValue());
    }
}
