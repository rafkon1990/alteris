<?php

namespace App\Tests\Alteris\Domain\MaterialGroup\ValueObject;

use App\Alteris\Domain\Material\Exception\InvalidPositionsValueException;
use App\Alteris\Domain\MaterialGroup\ValueObject\MoveUpPositions;
use PHPUnit\Framework\TestCase;

class MoveUpPositionsTest extends TestCase
{
    /**
     * @throws InvalidPositionsValueException
     */
    public function testValid(): void
    {
        $vo = new MoveUpPositions(1);
        $this->assertEquals(1, $vo->getValue());
    }

    /**
     * @throws InvalidPositionsValueException
     */
    public function testInvalid(): void
    {
        $this->expectException(InvalidPositionsValueException::class);
        new MoveUpPositions(-1);
    }

    /**
     * @throws InvalidPositionsValueException
     */
    public function testZero(): void
    {
        $this->expectException(InvalidPositionsValueException::class);
        new MoveUpPositions(0);
    }
}
