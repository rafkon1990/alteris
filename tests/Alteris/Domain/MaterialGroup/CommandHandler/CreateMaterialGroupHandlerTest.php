<?php

namespace App\Tests\Alteris\Domain\MaterialGroup\CommandHandler;

use App\Alteris\Domain\MaterialGroup\Factory\MaterialGroupFactory;
use App\Alteris\Domain\MaterialGroup\Command\CreateMaterialGroup;
use App\Alteris\Domain\MaterialGroup\CommandHandler\CreateMaterialGroupHandler;
use App\Alteris\Domain\MaterialGroup\Exception\IdNotUniqueException;
use App\Alteris\Domain\MaterialGroup\Exception\NameMaxException;
use App\Alteris\Domain\MaterialGroup\Exception\NameMinException;
use App\Alteris\Domain\MaterialGroup\Exception\ParentNotFoundException;
use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;
use App\Alteris\Domain\MaterialGroup\Repository\MaterialGroupRepositoryInterface;
use App\Alteris\Domain\MaterialGroup\ValueObject\Name;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use App\Alteris\Infrastructure\Persistence\InMemory\MaterialGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class CreateMaterialGroupHandlerTest extends TestCase
{
    /** @var MaterialGroupRepositoryInterface */
    private $repository;

    /** @var MaterialGroupFactory */
    private $factory;

    /** @var MaterialGroup */
    private $root;

    protected function setUp(): void
    {
        $this->repository = new MaterialGroupRepository();
        $this->factory = new MaterialGroupFactory();
        $root = $this->createMock(MaterialGroup::class);
        $root->method('getId')->willReturn('425d93ff-c124-49f5-b75f-a557cae90c13');
        $root->method('getName')->willReturn('root');
        $this->repository->save($root);
        $this->root = $root;
    }

    /**
     * @throws IdNotUniqueException
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ParentNotFoundException
     */
    public function testUuidNotUnique(): void
    {
        $id = 'a5522e67-e384-491d-8c4a-bed62a7246a1';
        $this->repository->save(new MaterialGroup(new Uuid($id), new Name('test'), new ArrayCollection([]),
            $this->root));
        $handler = new CreateMaterialGroupHandler($this->repository, $this->factory);
        $command = new CreateMaterialGroup($id, 'test2', $this->root->getId());
        $this->expectException(IdNotUniqueException::class);
        $handler($command);
    }

    /**
     * @throws IdNotUniqueException
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ParentNotFoundException
     */
    public function testValid(): void
    {
        $handler = new CreateMaterialGroupHandler($this->repository, $this->factory);
        $command = new CreateMaterialGroup('a5522e67-e384-491d-8c4a-bed62a7246a1', 'meter', $this->root->getId());
        $handler($command);
        $this->assertNotNull($this->repository->findById('a5522e67-e384-491d-8c4a-bed62a7246a1'));
    }
}
