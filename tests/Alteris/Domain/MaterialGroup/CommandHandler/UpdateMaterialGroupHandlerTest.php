<?php

namespace App\Tests\Alteris\Domain\MaterialGroup\CommandHandler;

use App\Alteris\Domain\MaterialGroup\Command\UpdateMaterialGroup;
use App\Alteris\Domain\MaterialGroup\CommandHandler\UpdateMaterialGroupHandler;
use App\Alteris\Domain\MaterialGroup\Exception\InvalidParentException;
use App\Alteris\Domain\MaterialGroup\Exception\MaterialGroupNotFoundException;
use App\Alteris\Domain\MaterialGroup\Exception\NameMaxException;
use App\Alteris\Domain\MaterialGroup\Exception\NameMinException;
use App\Alteris\Domain\MaterialGroup\Exception\ParentNotFoundException;
use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;
use App\Alteris\Domain\MaterialGroup\Repository\MaterialGroupRepositoryInterface;
use App\Alteris\Domain\MaterialGroup\ValueObject\Name;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use App\Alteris\Infrastructure\Persistence\InMemory\MaterialGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class UpdateMaterialGroupHandlerTest extends TestCase
{
    /** @var MaterialGroupRepositoryInterface */
    private $repository;

    /** @var string */
    private $existingUuidValue1 = '6bc49d57-e865-4e19-b06c-955f2c0d0ca8';

    /** @var string */
    private $existingUuidValue2 = '731a7db6-3ceb-4ad2-97f8-9066ea823d62';

    /** @var string */
    private $nonExistingUuidValue = 'd59c1240-b2f9-4518-aa16-efd1a7a65dcb';

    /** @var MaterialGroup */
    private $root;

    protected function setUp(): void
    {
        $this->repository = new MaterialGroupRepository();
        $root = $this->createMock(MaterialGroup::class);
        $root->method('getId')->willReturn('425d93ff-c124-49f5-b75f-a557cae90c13');
        $root->method('getName')->willReturn('root');
        $this->repository->save($root);
        $this->root = $root;
        $this->repository->save(
            new MaterialGroup(new Uuid($this->existingUuidValue1), new Name('Food'), new ArrayCollection([]), $root),
            new MaterialGroup(new Uuid($this->existingUuidValue2), new Name('Water'), new ArrayCollection([]), $root)
        );
    }

    /**
     * @throws InvalidParentException
     * @throws MaterialGroupNotFoundException
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ParentNotFoundException
     */
    public function testUpdateNotExisting(): void
    {
        $handler = new UpdateMaterialGroupHandler($this->repository);
        $command = new UpdateMaterialGroup($this->nonExistingUuidValue, 'Drink', $this->root->getId());
        $this->expectException(MaterialGroupNotFoundException::class);
        $handler($command);
    }

    /**
     * @throws MaterialGroupNotFoundException
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ParentNotFoundException
     * @throws InvalidParentException
     */
    public function testUpdateNotExistingParent(): void
    {
        $handler = new UpdateMaterialGroupHandler($this->repository);
        $command = new UpdateMaterialGroup($this->nonExistingUuidValue, 'Drink', $this->nonExistingUuidValue);
        $this->expectException(MaterialGroupNotFoundException::class);
        $handler($command);
    }

    /**
     * @throws MaterialGroupNotFoundException
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ParentNotFoundException
     * @throws InvalidParentException
     */
    public function testUpdateName(): void
    {
        $handler = new UpdateMaterialGroupHandler($this->repository);
        $command = new UpdateMaterialGroup($this->existingUuidValue1, 'Drink', $this->root->getId());
        $handler($command);
        $model = $this->repository->findById($this->existingUuidValue1);
        $this->assertNotNull($model);
        $this->assertEquals('Drink', $model->getName());
    }

    /**
     * @throws MaterialGroupNotFoundException
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ParentNotFoundException
     * @throws InvalidParentException
     */
    public function testUpdateParent(): void
    {
        $parentIdBeforeUpdate = $this->repository->findById($this->existingUuidValue2)->getParent()->getId();
        $this->assertEquals($parentIdBeforeUpdate, $this->root->getId());
        $handler = new UpdateMaterialGroupHandler($this->repository);
        $command = new UpdateMaterialGroup($this->existingUuidValue2, 'Drink', $this->existingUuidValue1);
        $handler($command);
        $model = $this->repository->findById($this->existingUuidValue2);
        $this->assertNotNull($model);
        $this->assertNotEquals($parentIdBeforeUpdate, $model->getParent()->getId());
        $this->assertNotEquals($model->getParent()->getId(), $this->root->getId());
    }
}
