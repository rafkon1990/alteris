<?php

namespace App\Tests\Alteris\Application\Api\Controller;

use App\Tests\Alteris\Application\DataFixtures\AppFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MaterialControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function testCreateWithoutAnyData(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_POST, '/api/material', []);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testCreateWithoutName(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_POST, '/api/material', ['code' => 'test']);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testWithValidData(): void
    {
        $this->loadFixtures([AppFixtures::class]);
        $client = static::createClient();
        $client->request(Request::METHOD_POST, '/api/material', ['name' => 'Test', 'code' => 't', 'material_group_id' => AppFixtures::MATERIAL_GROUP_METAL_ID, 'measure_unit_id' => AppFixtures::MEASURE_UNIT_VOLUME_ID]);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testGetAll(): void
    {
        $this->loadFixtures([AppFixtures::class]);
        $client = $this->createClient();
        $client->request(Request::METHOD_GET, '/api/measure_units');
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $responseBody = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEquals([], $responseBody); // there should be fixtures
    }

    public function testGetOne(): void
    {
        $this->loadFixtures([AppFixtures::class]);
        $client = $this->createClient();
        $client->request(Request::METHOD_GET, '/api/material/' . AppFixtures::MATERIAL_MERCURY_ID);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $responseBody = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals([
            'id' => AppFixtures::MATERIAL_MERCURY_ID,
            'code' => 'Hg',
            'name' => 'Mercury',
            'material_group' => [
                'id' => AppFixtures::MATERIAL_GROUP_METAL_ID,
                'name' => 'Metal',
            ],
            'measure_unit' => [
                'id' => AppFixtures::MEASURE_UNIT_VOLUME_ID,
                'name' => 'volume',
                'short_name' => 'V',
            ],
        ], $responseBody);
    }
}
