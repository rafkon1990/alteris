<?php

namespace App\Tests\Alteris\Application\Api\Controller;

use App\Tests\Alteris\Application\DataFixtures\AppFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MeasureUnitControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function testCreateWithoutAnyData(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_POST, '/api/measure_unit', []);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testCreateWithoutName(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_POST, '/api/measure_unit', ['short_name' => 'm.']);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testValidData(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_POST, '/api/measure_unit', ['name' => 'meter', 'short_name' => 'm.']);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testGetAll(): void
    {
        $this->loadFixtures([AppFixtures::class]);
        $client = $this->createClient();
        $client->request(Request::METHOD_GET, '/api/measure_units');
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $responseBody = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEquals([], $responseBody); // there should be fixtures
    }
}
