<?php

namespace App\Tests\Alteris\Application\Api\Controller;

use App\Tests\Alteris\Application\DataFixtures\AppFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MaterialGroupControllerTest extends WebTestCase
{
    use FixturesTrait;

    protected function setUp(): void
    {
        $this->loadFixtures([AppFixtures::class]);
    }

    public function testCreateWithoutAnyData(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_POST, '/api/material_group', []);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testCreateWithoutName(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_POST, '/api/material_group',
            ['parent_id' => AppFixtures::MATERIAL_GROUP_ROOT_ID]);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testCreateWithValidData(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_POST, '/api/material_group',
            ['name' => 'Stone', 'parent_id' => AppFixtures::MATERIAL_GROUP_ROOT_ID]);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testGetAll(): void
    {
        $client = self::createClient();
        $client->request(Request::METHOD_GET, '/api/material_groups');
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $responseBody = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEquals([], $responseBody); // there should be fixtures
    }

    public function testGetOne(): void
    {
        $client = self::createClient();
        $client->request(Request::METHOD_GET, '/api/material_group/' . AppFixtures::MATERIAL_GROUP_POLYMER_ID);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $responseBody = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEquals([], $responseBody); // there should be fixtures
    }

    public function testCanMoveUp(): void
    {
        $client = static::createClient();
        $stoneId = $this->createMaterialGroup($client, 'Stone', AppFixtures::MATERIAL_GROUP_ROOT_ID)['id'];
        $igneousData = $this->createMaterialGroup($client, 'Igneous', $stoneId);
        $surfaceData = $this->createMaterialGroup($client, 'Surface', $igneousData['id']);
        $metamorphicData = $this->createMaterialGroup($client, 'Metamorphic', $stoneId);

        $client->request(Request::METHOD_GET, '/api/material_group/tree/' . $stoneId,
            ['name' => 'Metamorphic', 'parent_id' => $stoneId]);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals([
            'id' => $stoneId,
            'name' => 'Stone',
            'children' => [
                [
                    'id' => $igneousData['id'],
                    'name' => $igneousData['name'],
                    'children' => [
                        [
                            'id' => $surfaceData['id'],
                            'name' => $surfaceData['name'],
                            'children' => [],
                        ],
                    ],
                ],
                [
                    'id' => $metamorphicData['id'],
                    'name' => $metamorphicData['name'],
                    'children' => [
                    ],
                ],
            ],
        ],
            $data);

        $client->request(Request::METHOD_PUT, sprintf('/api/material_group/move_up/%s/%d', $metamorphicData['id'], 1),);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $client->request(Request::METHOD_GET, '/api/material_group/tree/' . $stoneId,
            ['name' => 'Metamorphic', 'parent_id' => $stoneId]);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals([
            'id' => $stoneId,
            'name' => 'Stone',
            'children' => [
                [
                    'id' => $metamorphicData['id'],
                    'name' => $metamorphicData['name'],
                    'children' => [
                    ],
                ],
                [
                    'id' => $igneousData['id'],
                    'name' => $igneousData['name'],
                    'children' => [
                        [
                            'id' => $surfaceData['id'],
                            'name' => $surfaceData['name'],
                            'children' => [],
                        ],
                    ],
                ],
            ],
        ],
            $data);
    }

    private function createMaterialGroup(KernelBrowser $client, string $name, string $parentId): array
    {
        $client->request(Request::METHOD_POST, '/api/material_group',
            ['name' => $name, 'parent_id' => $parentId]);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $metamorphicData = json_decode($client->getResponse()->getContent(), true);

        return $metamorphicData;
    }
}
