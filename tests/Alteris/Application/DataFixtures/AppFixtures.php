<?php

namespace App\Tests\Alteris\Application\DataFixtures;

use App\Alteris\Domain\Material\Model\Material;
use App\Alteris\Domain\Material\ValueObject\Code;
use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;
use App\Alteris\Domain\MaterialGroup\ValueObject\Name;
use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit;
use App\Alteris\Domain\MeasureUnit\ValueObject\ShortName;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    // TODO: refactor the following ids
    public const MATERIAL_GROUP_ROOT_ID = 'aec56abe-29a3-4a94-8510-002219e548f1';
    public const MATERIAL_GROUP_PLASTIC_ID = 'b17c4701-cded-4baa-bce1-ae0dbabaa431';
    public const MATERIAL_GROUP_POLYMER_ID = '911970d4-0b45-4b55-9c35-8e7abea24c5c';
    public const MATERIAL_GROUP_METAL_ID = 'cfbc9d6a-6757-490e-a442-4c6163f3bba7';
    public const MATERIAL_GROUP_CERAMIC_ID = '99378032-2b64-4b4f-80ef-a73b7cffb38e';
    public const MEASURE_UNIT_VOLUME_ID = 'cea18305-7c54-4f5f-935b-4cc6d092bed8';
    public const MEASURE_UNIT_WEIGHT_ID = '63eb1783-ab58-4ff3-9793-94d3bc325aa3';
    public const MATERIAL_MERCURY_ID = 'f92252aa-aa7c-4e66-88a1-2966a08fb8e2';
    public const MATERIAL_STYROFOAM_ID = '14486c5e-cc59-4486-9ae1-9b969aa1b433';


    public function load(ObjectManager $manager)
    {
        $manager->persist($root = new MaterialGroup(new Uuid(self::MATERIAL_GROUP_ROOT_ID), new Name('root'),
            new ArrayCollection([]), null));
        $manager->persist($polymer = new MaterialGroup(new Uuid(self::MATERIAL_GROUP_POLYMER_ID), new Name('Polymer'),
            new ArrayCollection([]), $root));
        $manager->persist($plastic = new MaterialGroup(new Uuid(self::MATERIAL_GROUP_PLASTIC_ID), new Name('Plastic'),
            new ArrayCollection([]), $polymer));
        $manager->persist($metal = new MaterialGroup(new Uuid(self::MATERIAL_GROUP_METAL_ID), new Name('Metal'),
            new ArrayCollection([]), $root));
        $manager->persist($ceramic = new MaterialGroup(new Uuid(self::MATERIAL_GROUP_CERAMIC_ID), new Name('Ceramic'),
            new ArrayCollection([]), $root));

        $manager->persist($volume = new MeasureUnit(new Uuid(self::MEASURE_UNIT_VOLUME_ID),
            new \App\Alteris\Domain\MeasureUnit\ValueObject\Name('volume'), new ShortName('V')));
        $manager->persist($weight = new MeasureUnit(new Uuid(self::MEASURE_UNIT_WEIGHT_ID),
            new \App\Alteris\Domain\MeasureUnit\ValueObject\Name('weight'), new ShortName('m.')));

        $manager->persist(new Material(new Uuid(self::MATERIAL_MERCURY_ID), new Code('Hg'),
            new \App\Alteris\Domain\Material\ValueObject\Name('Mercury'), $metal, $volume));
        $manager->persist(new Material(new Uuid(self::MATERIAL_STYROFOAM_ID), new Code('Styrofoam'),
            new \App\Alteris\Domain\Material\ValueObject\Name('Styrofoam'), $polymer, $volume));


        $manager->flush();
    }
}
