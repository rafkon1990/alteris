# Środowisko dockerowe

Uruchamiamy za pomocą docker-compose. budując ```docker-compose build``` a następnie ```docker-compose up```

Wykonaj migracje doctrinowe, np. komendą: ```docker exec -it alteris_php_1 /var/www/symfony/bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration```

# Testy automatyczne

Projekt zawiera pełen zestaw testów automatycznych http://q.i-systems.pl/file/420fd350.png, które można uruchomić wewnątrz konteneru lub za pomocą interpretera php z obrazem dockerowym:
http://q.i-systems.pl/file/ff6c108d.png http://q.i-systems.pl/file/606fe88f.png http://q.i-systems.pl/file/574971ee.png
# xDebug

Projekt można swobodnie debugować, wystarczy skonfigurować xDebug w IDE: http://q.i-systems.pl/file/53eec1e4.png http://q.i-systems.pl/file/3f282ee8.png

# TODO
1. Dodanie read modelu
2. Dodanie dokumentacji API
