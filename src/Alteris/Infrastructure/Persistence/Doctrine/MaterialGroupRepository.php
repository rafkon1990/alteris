<?php

namespace App\Alteris\Infrastructure\Persistence\Doctrine;

use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup as Entity;
use App\Alteris\Domain\MaterialGroup\ReadRepository\MaterialGroupRepositoryInterface as MaterialGroupReadRepositoryInterfaceAlias;
use App\Alteris\Domain\MaterialGroup\Repository\MaterialGroupRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * @method Entity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity[]    findAll()
 * @method Entity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterialGroupRepository extends NestedTreeRepository implements MaterialGroupRepositoryInterface, MaterialGroupReadRepositoryInterfaceAlias
{
    public function __construct(EntityManagerInterface $manager)
    {
        parent::__construct($manager, $manager->getClassMetadata(Entity::class));
    }

    public function findById(string $id): ?Entity
    {
        return $this->find($id);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Entity... $entities): void
    {
        if (count($entities) > 0) {
            foreach ($entities as $entity) {
                $this->getEntityManager()->persist($entity);
            }
            $this->getEntityManager()->flush();
        }
    }

    public function moveBy(Entity $entity, int $positions): void
    {
        $this->moveUp($entity, $positions);
    }

    public function getChildrenHierarchy(?Entity $entity): array
    {
        return $this->childrenHierarchy($entity);
    }

    public function findRoot(): ?Entity
    {
        $roots = $this->getRootNodes();
        $root = reset($roots);

        return $root ?? null;
    }

    public function hasChildren(string $id): bool
    {
        $entity = $this->findById($id);
        if ($entity === null) {
            return false; // not exist so has not
        }

        return $this->childCount($entity) > 0;
    }
}
