<?php

namespace App\Alteris\Infrastructure\Persistence\InMemory;

use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup as Entity;
use App\Alteris\Domain\MaterialGroup\Repository\MaterialGroupRepositoryInterface;
use RuntimeException;

class MaterialGroupRepository implements MaterialGroupRepositoryInterface
{
    private $memory = [];

    public function __construct(Entity... $entities)
    {
        foreach ($entities as $entity) {
            $this->memory[$entity->getId()] = $entity;
        }
    }

    public function findById(string $id): ?Entity
    {
        return $this->memory[$id] ?? null;
    }

    public function save(Entity... $entities): void
    {
        foreach ($entities as $entity) {
            $this->memory[$entity->getId()] = $entity;
        }
    }

    public function moveBy(Entity $entity, int $positions): void
    {
        throw new RuntimeException('Unsupported method.');
    }
}
