<?php

namespace App\Alteris\Infrastructure\Persistence\InMemory;

use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit as Entity;
use App\Alteris\Domain\MeasureUnit\Repository\MeasureUnitRepositoryInterface;

class MeasureUnitRepository implements MeasureUnitRepositoryInterface
{
    private $memory = [];

    public function __construct(Entity... $entities)
    {
        foreach ($entities as $entity) {
            $this->memory[$entity->getId()] = $entity;
        }
    }

    public function findById(string $id): ?Entity
    {
        return $this->memory[$id] ?? null;
    }

    public function findAll()
    {
        return array_values($this->memory);
    }

    public function save(Entity $entity): void
    {
        $this->memory[$entity->getId()] = $entity;
    }
}
