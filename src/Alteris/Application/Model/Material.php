<?php

namespace App\Alteris\Application\Model;

class Material
{
    /** @var string */
    private $id;

    /** @var string */
    private $code;

    /** @var string */
    private $name;

    /** @var MaterialGroup */
    private $materialGroupId;

    /** @var MeasureUnit */
    private $measureUnitId;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getMaterialGroupId(): ?string
    {
        return $this->materialGroupId;
    }

    public function setMaterialGroupId(string $materialGroupId): void
    {
        $this->materialGroupId = $materialGroupId;
    }

    public function getMeasureUnitId(): ?string
    {
        return $this->measureUnitId;
    }

    public function setMeasureUnitId(string $measureUnitId): void
    {
        $this->measureUnitId = $measureUnitId;
    }
}
