<?php

namespace App\Alteris\Application\Model;

class MeasureUnit
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $shortName;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): void
    {
        $this->shortName = $shortName;
    }
}
