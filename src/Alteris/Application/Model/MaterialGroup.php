<?php

namespace App\Alteris\Application\Model;

class MaterialGroup
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $parentId;

    /** @var int */
    private $positions;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getParentId(): ?string
    {
        return $this->parentId;
    }

    public function setParentId(string $parentId): void
    {
        $this->parentId = $parentId;
    }

    public function getPositions(): ?int
    {
        return $this->positions;
    }

    public function setPositions(int $positions): void
    {
        $this->positions = $positions;
    }
}
