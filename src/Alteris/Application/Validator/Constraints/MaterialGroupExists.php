<?php

namespace App\Alteris\Application\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class MaterialGroupExists extends Constraint
{
    public $message;

    public function __construct(string $message = 'alteris.material_group.id.not_found')
    {
        $this->message = $message;
        parent::__construct();
    }
}
