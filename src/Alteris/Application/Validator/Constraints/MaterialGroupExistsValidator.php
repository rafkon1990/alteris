<?php

namespace App\Alteris\Application\Validator\Constraints;

use App\Alteris\Domain\MaterialGroup\Query\FindMaterialGroupById;
use App\Alteris\Infrastructure\Common\MessageHandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MaterialGroupExistsValidator extends ConstraintValidator
{
    use MessageHandleTrait;

    /** @var MessageBusInterface */
    private $queryBus;

    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (empty($value)) {
            $this->context->buildViolation($constraint->message)->addViolation();

            return;
        }

        $model = $this->handleMessage($this->queryBus, new FindMaterialGroupById($value));
        if ($model === null) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
