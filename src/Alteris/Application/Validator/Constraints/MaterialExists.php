<?php

namespace App\Alteris\Application\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class MaterialExists extends Constraint
{
    public $message;

    public function __construct(string $message = 'alteris.material.id.not_found')
    {
        $this->message = $message;
        parent::__construct();
    }
}
