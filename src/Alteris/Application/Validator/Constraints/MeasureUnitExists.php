<?php

namespace App\Alteris\Application\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class MeasureUnitExists extends Constraint
{
    public $message;

    public function __construct(string $message = 'alteris.measure_unit.id.not_found')
    {
        $this->message = $message;
        parent::__construct();
    }
}
