<?php

namespace App\Alteris\Application\Validator\Constraints;

use App\Alteris\Domain\MeasureUnit\Query\FindMeasureUnitById;
use App\Alteris\Infrastructure\Common\MessageHandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MeasureUnitExistsValidator extends ConstraintValidator
{
    use MessageHandleTrait;

    /** @var MessageBusInterface */
    private $queryBus;

    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (empty($value)) {
            $this->context->buildViolation($constraint->message)->addViolation();

            return;
        }

        $model = $this->handleMessage($this->queryBus, new FindMeasureUnitById($value));
        if ($model === null) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
