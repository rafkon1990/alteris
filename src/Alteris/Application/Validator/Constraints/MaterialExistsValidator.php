<?php

namespace App\Alteris\Application\Validator\Constraints;

use App\Alteris\Domain\Material\Query\FindMaterialById;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MaterialExistsValidator extends ConstraintValidator
{
    /** @var MessageBusInterface */
    private $queryBus;

    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function validate($value, Constraint $constraint): void
    {
        $model = $this->queryBus->dispatch(new FindMaterialById($value));
        if ($model === null) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
