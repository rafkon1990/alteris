<?php

namespace App\Alteris\Application\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class MaterialGroupHasNotChildren extends Constraint
{
    public $message = 'alteris.material_group.children.has_children';
}
