<?php

namespace App\Alteris\Application\Api\Model;

class MeasureUnit
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $shortName;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): MeasureUnit
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): MeasureUnit
    {
        $this->name = $name;

        return $this;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): MeasureUnit
    {
        $this->shortName = $shortName;

        return $this;
    }
}
