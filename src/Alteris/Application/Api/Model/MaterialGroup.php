<?php

namespace App\Alteris\Application\Api\Model;

class MaterialGroup
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): MaterialGroup
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): MaterialGroup
    {
        $this->name = $name;

        return $this;
    }
}
