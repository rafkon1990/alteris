<?php

namespace App\Alteris\Application\Api\Model;

class MaterialGroupNode
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var MaterialGroupNode[] */
    private $children = [];

    public function __construct(string $id, string $name, array $children)
    {
        $this->id = $id;
        $this->name = $name;
        foreach ($children as $child) {
            $this->addChild($child);
        }
    }

    public function addChild(MaterialGroupNode $node): self
    {
        $this->children[] = $node;

        return $this;
    }
}
