<?php

namespace App\Alteris\Application\Api\Model;

class Material
{
    /** @var string */
    private $id;

    /** @var string */
    private $code;

    /** @var string */
    private $name;

    /** @var MaterialGroup */
    private $materialGroup;

    /** @var MeasureUnit */
    private $measureUnit;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Material
    {
        $this->id = $id;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): Material
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Material
    {
        $this->name = $name;

        return $this;
    }

    public function getMaterialGroup(): MaterialGroup
    {
        return $this->materialGroup;
    }

    public function setMaterialGroup(MaterialGroup $materialGroup): Material
    {
        $this->materialGroup = $materialGroup;

        return $this;
    }

    public function getMeasureUnit(): MeasureUnit
    {
        return $this->measureUnit;
    }

    public function setMeasureUnit(MeasureUnit $measureUnit): Material
    {
        $this->measureUnit = $measureUnit;

        return $this;
    }
}
