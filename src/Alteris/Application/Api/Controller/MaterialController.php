<?php

namespace App\Alteris\Application\Api\Controller;

use App\Alteris\Application\Api\Factory\MaterialFactory;
use App\Alteris\Application\Form\CreateMaterialType;
use App\Alteris\Application\Form\UpdateMaterialType;
use App\Alteris\Application\Model\Material;
use App\Alteris\Domain\Material\Command\CreateMaterial;
use App\Alteris\Domain\Material\Command\UpdateMaterial;
use App\Alteris\Domain\Material\Query\FindAllMaterials;
use App\Alteris\Domain\Material\Query\FindMaterialById;
use App\Alteris\Infrastructure\Common\MessageHandleTrait;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

/**
 * @Route("/api",name="api_")
 */
class MaterialController extends AbstractFOSRestController
{
    use MessageHandleTrait;

    /** @var MessageBusInterface */
    private $messageBus;

    /** @var MaterialFactory */
    private $factory;

    public function __construct(MessageBusInterface $messageBus, MaterialFactory $factory)
    {
        $this->messageBus = $messageBus;
        $this->factory = $factory;
    }

    private function getModel(string $id = null): Material
    {
        $material = new Material();
        $material->setId($id ?? uuid_create(UUID_TYPE_DCE));

        return $material;
    }

    /**
     * @Rest\Get("/material/{id}")
     */
    public function getMaterialAction(string $id): Response
    {
        $model = $this->handleMessage($this->messageBus, new FindMaterialById($id));

        if ($model === null) {
            throw $this->createNotFoundException();
        }

        return $this->handleView($this->view($this->factory->create($model)));
    }

    /**
     * @Rest\Get("/materials")
     */
    public function getMaterialsAction(): Response
    {
        // TODO: handle pagination?
        $models = $this->handleMessage($this->messageBus, new FindAllMaterials());
        $viewModels = [];

        foreach ($models as $model) {
            $viewModels[] = $this->factory->create($model);
        }

        return $this->handleView($this->view($viewModels));
    }

    /**
     * @Rest\Post("/material")
     */
    public function createMaterialAction(Request $request): Response
    {
        $model = $this->getModel($id = uuid_create(UUID_TYPE_DCE));
        $form = $this->createForm(CreateMaterialType::class, $model);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $command = new CreateMaterial(
                $id, $model->getCode(), $model->getName(), $model->getMaterialGroupId(), $model->getMeasureUnitId()
            );

            try {
                $this->messageBus->dispatch($command);
            } catch (Throwable $throwable) {
                throw new HttpException(500, 'Internal server error');
            }
        } else {
            return $this->handleView($this->view($form));
        }

        return $this->handleView($this->view($model));
    }

    /**
     * @Rest\Put("/material/{id}")
     */
    public function updateMaterialAction(string $id, Request $request): Response
    {
        $model = $this->getModel($id);
        $form = $this->createForm(UpdateMaterialType::class, $model);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $command = new UpdateMaterial(
                $model->getId(), $model->getCode(), $model->getName(), $model->getMaterialGroupId(), $model->getMeasureUnitId()
            );

            try {
                $this->messageBus->dispatch($command);
            } catch (Throwable $throwable) {
                throw new HttpException(500, 'Internal server error');
            }
        }

        return $this->handleView($this->view($model));
    }
}
