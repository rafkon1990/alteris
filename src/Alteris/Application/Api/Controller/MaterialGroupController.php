<?php

namespace App\Alteris\Application\Api\Controller;

use App\Alteris\Application\Api\Factory\MaterialGroupFactory;
use App\Alteris\Application\Api\Factory\MaterialGroupTreeFactory;
use App\Alteris\Application\Form\CreateMaterialGroupType;
use App\Alteris\Application\Form\MoveDownMaterialGroupType;
use App\Alteris\Application\Form\MoveUpMaterialGroupType;
use App\Alteris\Application\Form\UpdateMeasureUnitType;
use App\Alteris\Application\Model\MaterialGroup;
use App\Alteris\Domain\MaterialGroup\Command\CreateMaterialGroup;
use App\Alteris\Domain\MaterialGroup\Command\MoveDownMaterialGroup;
use App\Alteris\Domain\MaterialGroup\Command\MoveUpMaterialGroup;
use App\Alteris\Domain\MaterialGroup\Query\FindAllMaterialGroups;
use App\Alteris\Domain\MaterialGroup\Query\FindMaterialGroupById;
use App\Alteris\Domain\MaterialGroup\Query\FindMaterialGroupRoot;
use App\Alteris\Domain\MaterialGroup\Query\GetMaterialGroupChildren;
use App\Alteris\Domain\MeasureUnit\Command\UpdateMeasureUnit;
use App\Alteris\Infrastructure\Common\MessageHandleTrait;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ValidatorBuilder;
use Throwable;

/**
 * @Route("/api",name="api_")
 */
class MaterialGroupController extends AbstractFOSRestController
{
    use MessageHandleTrait;

    /** @var MessageBusInterface */
    private $messageBus;

    /** @var MaterialGroupFactory */
    private $factory;

    /** @var MaterialGroupTreeFactory */
    private $treeFactory;

    public function __construct(
        MessageBusInterface $messageBus,
        MaterialGroupFactory $factory,
        MaterialGroupTreeFactory $treeFactory
    ) {
        $this->messageBus = $messageBus;
        $this->factory = $factory;
        $this->treeFactory = $treeFactory;
    }

    private function getModel(string $id): MaterialGroup
    {
        $model = new MaterialGroup();
        $model->setId($id);

        return $model;
    }

    /**
     * @Rest\Get("/material_group/{id}")
     */
    public function getMaterialGroupAction(string $id): Response
    {
        $model = $this->handleMessage($this->messageBus, new FindMaterialGroupById($id));
        if ($model === null) {
            throw $this->createNotFoundException();
        }

        return $this->handleView($this->view($this->factory->create($model)));
    }

    /**
     * @Rest\Get("/material_groups")
     */
    public function getMaterialGroupsAction(): Response
    {
        $models = $this->handleMessage($this->messageBus, new FindAllMaterialGroups());
        $viewModels = [];

        foreach ($models as $model) {
            $viewModels[] = $this->factory->create($model);
        }

        return $this->handleView($this->view($viewModels));
    }

    /**
     * @Rest\Post("/material_group")
     */
    public function createMaterialGroupAction(Request $request): Response
    {
        $model = $this->getModel($id = uuid_create(UUID_TYPE_DCE));
        $form = $this->createForm(CreateMaterialGroupType::class, $model);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $command = new CreateMaterialGroup($model->getId(), $model->getName(), $model->getParentId());

            try {
                $this->messageBus->dispatch($command);
            } catch (Throwable $throwable) {
                throw new HttpException(500, 'Internal server error');
            }
        } else {
            return $this->handleView($this->view($form));
        }

        return $this->handleView($this->view($model));
    }

    /**
     * @Rest\Put("/material_group/{id}")
     */
    public function updateMaterialGroupAction(string $id, Request $request): Response
    {
        $model = $this->getModel($id);
        $form = $this->createForm(UpdateMeasureUnitType::class, $model);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $command = new UpdateMeasureUnit($id, $model->getName(), $model->getParentId());

            try {
                $this->messageBus->dispatch($command);
            } catch (Throwable $throwable) {
                throw new HttpException(500, 'Internal server error');
            }
        } else {
            return $this->handleView($this->view($form));
        }

        return $this->handleView($this->view($model));
    }

    /**
     * @Rest\Put("/material_group/move_up/{id}/{positions}")
     * @Rest\QueryParam(name="positions", requirements="\d+")
     */
    public function moveUpAction(string $id, int $positions, Request $request): Response
    {
        $model = $this->getModel($id);
        $form = $this->createForm(MoveUpMaterialGroupType::class, $model);
        $data = $request->request->all();
        $data['id'] = $id; // allow form to validate id
        $data['positions'] = $positions; // allow form to validate id
        $form->submit($data);

        if ($form->isValid()) {
            $command = new MoveUpMaterialGroup($id, $model->getPositions());

            try {
                $this->messageBus->dispatch($command);
            } catch (Throwable $throwable) {
                throw new HttpException(500, 'Internal server error');
            }
        } else {
            return $this->handleView($this->view($form));
        }

        return $this->handleView($this->view($model));
    }

    /**
     * @Rest\Put("/material_group/move_down/{id}/{positions}")
     */
    public function moveDownAction(string $id, int $positions, Request $request): Response
    {
        $model = $this->getModel($id);
        $form = $this->createForm(MoveDownMaterialGroupType::class, $model);
        $data = $request->request->all();
        $data['id'] = $id; // allow form to validate id
        $data['positions'] = $positions; // allow form to validate id
        $form->submit($data);

        if ($form->isValid()) {
            $command = new MoveDownMaterialGroup($id, $model->getPositions());

            try {
                $this->messageBus->dispatch($command);
            } catch (Throwable $throwable) {
                throw new HttpException(500, 'Internal server error');
            }
        } else {
            return $this->handleView($this->view($form));
        }

        return $this->handleView($this->view($model));
    }

    /**
     * @Rest\Get("/material_group/tree/{id}")
     * @Rest\QueryParam(name="id", default=null)
     */
    public function getTreeAction(?string $id = null): Response
    {
        if ($id) {
            $materialGroup = $this->handleMessage($this->messageBus, new FindMaterialGroupById($id));
        } else {
            $materialGroup = $this->handleMessage($this->messageBus, new FindMaterialGroupRoot());
        }
        $children = $this->handleMessage($this->messageBus, new GetMaterialGroupChildren($materialGroup));
        $model = $this->treeFactory->create($materialGroup, $children);

        return $this->handleView($this->view($model));
    }
}
