<?php

namespace App\Alteris\Application\Api\Controller;

use App\Alteris\Application\Api\Factory\MaterialFactory;
use App\Alteris\Application\Api\Factory\MeasureUnitFactory;
use App\Alteris\Application\Form\CreateMeasureUnitType;
use App\Alteris\Application\Form\UpdateMeasureUnitType;
use App\Alteris\Application\Model\MeasureUnit;
use App\Alteris\Domain\MeasureUnit\Command\CreateMeasureUnit;
use App\Alteris\Domain\MeasureUnit\Command\UpdateMeasureUnit;
use App\Alteris\Domain\MeasureUnit\Query\FindAllMeasureUnits;
use App\Alteris\Domain\MeasureUnit\Query\FindMeasureUnitById;
use App\Alteris\Infrastructure\Common\MessageHandleTrait;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

/**
 * @Route("/api",name="api_")
 */
class MeasureUnitController extends AbstractFOSRestController
{
    use MessageHandleTrait;

    /** @var MessageBusInterface */
    private $messageBus;

    /** @var MaterialFactory */
    private $factory;

    public function __construct(MessageBusInterface $messageBus, MeasureUnitFactory $factory)
    {
        $this->messageBus = $messageBus;
        $this->factory = $factory;
    }

    private function getModel(string $id): MeasureUnit
    {
        $model = new MeasureUnit();
        $model->setId($id);

        return $model;
    }

    /**
     * @Rest\Get("/measure_unit/{id}")
     */
    public function getMeasureUnitAction(string $id): Response
    {
        $model = $this->handleMessage($this->messageBus, new FindMeasureUnitById($id));

        if ($model === null) {
            throw $this->createNotFoundException();
        }

        return $this->handleView($this->view($this->factory->create($model)));
    }

    /**
     * @Rest\Get("/measure_units")
     */
    public function getMeasureUnitsAction(): Response
    {
        $models = $this->handleMessage($this->messageBus, new FindAllMeasureUnits());
        $viewModels = [];

        foreach ($models as $model) {
            $viewModels[] = $this->factory->create($model);
        }

        return $this->handleView($this->view($viewModels));
    }

    /**
     * @Rest\Post("/measure_unit")
     */
    public function createMeasureUnitAction(Request $request): Response
    {
        $model = $this->getModel($id = uuid_create(UUID_TYPE_DCE));
        $form = $this->createForm(CreateMeasureUnitType::class, $model);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $command = new CreateMeasureUnit($id, $model->getName(), $model->getShortName());

            try {
                $this->messageBus->dispatch($command);
            } catch (Throwable $throwable) {
                throw new HttpException(500, 'Internal server error');
            }
        } else {
            return $this->handleView($this->view($form));
        }

        return $this->handleView($this->view($model));
    }

    /**
     * @Rest\Put("/measure_unit/{id}")
     */
    public function updateMaterialAction(string $id, Request $request): Response
    {
        $model = $this->getModel($id);
        $form = $this->createForm(UpdateMeasureUnitType::class, $model);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $command = new UpdateMeasureUnit($model->getId(), $model->getName(), $model->getShortName());

            try {
                $this->messageBus->dispatch($command);
            } catch (Throwable $throwable) {
                throw new HttpException(500, 'Internal server error');
            }
        } else {
            return $this->handleView($this->view($form));
        }

        return $this->handleView($this->view($model));
    }
}
