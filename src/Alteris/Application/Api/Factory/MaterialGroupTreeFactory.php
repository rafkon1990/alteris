<?php

namespace App\Alteris\Application\Api\Factory;

use App\Alteris\Application\Api\Model\MaterialGroupNode;
use App\Alteris\Application\Api\Model\MaterialGroupTree;
use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;

class MaterialGroupTreeFactory
{
    public function create(MaterialGroup $rootNode, array $children): MaterialGroupTree
    {
        return new MaterialGroupTree($rootNode->getId(), $rootNode->getName(), $this->createNodes($children));
    }

    private function createNodes(array $children): array
    {
        $nodes = [];
        foreach ($children as $child) {
            $nodes[] = new MaterialGroupNode($child['id'], $child['name'], $this->createNodes($child['__children']));
        }

        return $nodes;
    }
}
