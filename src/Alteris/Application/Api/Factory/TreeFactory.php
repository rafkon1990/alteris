<?php

namespace App\Alteris\Application\Api\Factory;

use App\Alteris\Application\Api\Model\TreeDTO;
use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit;

class TreeFactory
{
    public function create(MeasureUnit $measureUnit): NodeDTO
    {
        $dto = new MeasureUnitDTO();
        $dto
            ->setId($measureUnit->getId())
            ->setName($measureUnit->getName())
            ->setShortName($measureUnit->getShortName());

        return $dto;
    }
}
