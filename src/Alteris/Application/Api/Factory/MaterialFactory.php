<?php

namespace App\Alteris\Application\Api\Factory;

use App\Alteris\Application\Api\Model\Material as MaterialDTO;
use App\Alteris\Domain\Material\Model\Material;

class MaterialFactory
{
    /** @var MaterialGroupFactory */
    private $materialGroupFactory;

    /** @var MeasureUnitFactory */
    private $measureUnitFactory;

    public function __construct(MaterialGroupFactory $materialGroupFactory, MeasureUnitFactory $measureUnitFactory)
    {
        $this->materialGroupFactory = $materialGroupFactory;
        $this->measureUnitFactory = $measureUnitFactory;
    }

    public function create(Material $material): MaterialDTO
    {
        $dto = new MaterialDTO();
        $dto->setId($material->getId())
            ->setCode($material->getCode())
            ->setName($material->getName())
            ->setMaterialGroup($this->materialGroupFactory->create($material->getMaterialGroup()))
            ->setMeasureUnit($this->measureUnitFactory->create($material->getMeasureUnit()));

        return $dto;
    }
}
