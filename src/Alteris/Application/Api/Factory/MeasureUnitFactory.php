<?php

namespace App\Alteris\Application\Api\Factory;

use App\Alteris\Application\Api\Model\MeasureUnit as MeasureUnitDTO;
use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit;

class MeasureUnitFactory
{
    public function create(MeasureUnit $measureUnit): MeasureUnitDTO
    {
        $dto = new MeasureUnitDTO();
        $dto
            ->setId($measureUnit->getId())
            ->setName($measureUnit->getName())
            ->setShortName($measureUnit->getShortName());

        return $dto;
    }
}
