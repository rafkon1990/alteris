<?php

namespace App\Alteris\Application\Api\Factory;

use App\Alteris\Application\Api\Model\MaterialGroup as MaterialGroupDTO;
use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;

class MaterialGroupFactory
{
    public function create(MaterialGroup $measureUnit): MaterialGroupDTO
    {
        $dto = new MaterialGroupDTO();
        $dto
            ->setId($measureUnit->getId())
            ->setName($measureUnit->getName());

        return $dto;
    }
}
