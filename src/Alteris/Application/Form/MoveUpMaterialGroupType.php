<?php

namespace App\Alteris\Application\Form;

use App\Alteris\Application\Model\MaterialGroup;
use App\Alteris\Application\Validator\Constraints\MaterialGroupExists;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Uuid;

class MoveUpMaterialGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => 'alteris.material_group.id.not_blank',]),
                    new Uuid(),
                    new MaterialGroupExists(),
                ],
            ])
            ->add('positions', IntegerType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => 'alteris.material_group.name.not_blank',]),
                    new Positive(['message' => 'alteris.material_group.position.positive',]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MaterialGroup::class,
            'csrf_protection' => false,
        ]);
    }
}
