<?php

namespace App\Alteris\Application\Form;

use App\Alteris\Application\Model\MaterialGroup;
use App\Alteris\Application\Validator\Constraints\MaterialExists;
use App\Alteris\Application\Validator\Constraints\MaterialGroupExists;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Uuid;

class UpdateMaterialGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => 'alteris.material_group.id.not_blank',]),
                    new Uuid(),
                    new MaterialExists(),
                ],
            ])
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => 'alteris.material_group.name.not_blank',]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'alteris.material_group.name.min',
                        'max' => 255,
                        'maxMessage' => 'alteris.material_group.name.max',
                    ]),
                ],
            ])
            ->add('parent_id', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(
                        ['message' => 'alteris.material_group.parent_id.not_blank',]
                    ),
                    new Uuid(),
                    new MaterialGroupExists('alteris.material_group.parent_id.not_found'),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MaterialGroup::class,
            'csrf_protection' => false,
        ]);
    }
}
