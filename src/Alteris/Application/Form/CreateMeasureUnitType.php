<?php

namespace App\Alteris\Application\Form;

use App\Alteris\Application\Model\MeasureUnit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateMeasureUnitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => 'alteris.measure_unit.name.not_blank',]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'alteris.measure_unit.name.min',
                        'max' => 255,
                        'maxMessage' => 'alteris.measure_unit.name.max',
                    ]),
                ],
            ])
            ->add('short_name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => 'alteris.measure_unit.short_name.not_blank',]),
                    new Length([
                        'min' => 1,
                        'minMessage' => 'alteris.measure_unit.short_name.min',
                        'max' => 5,
                        'maxMessage' => 'alteris.measure_unit.short_name.max',
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MeasureUnit::class,
            'csrf_protection' => false,
        ]);
    }
}
