<?php

namespace App\Alteris\Application\Form;

use App\Alteris\Application\Model\Material;
use App\Alteris\Application\Validator\Constraints\MaterialExists;
use App\Alteris\Application\Validator\Constraints\MaterialGroupExists;
use App\Alteris\Application\Validator\Constraints\MaterialGroupHasNotChildren;
use App\Alteris\Application\Validator\Constraints\MaterialGroupHasNotChildrenValidator;
use App\Alteris\Application\Validator\Constraints\MeasureUnitExists;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Uuid;

class UpdateMaterialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => 'alteris.material.id.not_blank',]),
                    new Uuid(),
                    new MaterialExists(),
                ],
            ])
            ->add('code', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => 'alteris.material.code.not_blank',]),
                    new Length([
                        'min' => 1,
                        'minMessage' => 'alteris.material.code.min',
                        'max' => 32,
                        'maxMessage' => 'alteris.material.code.max',
                    ]),
                ],
            ])
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => 'alteris.material.name.not_blank',]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'alteris.material.name.min',
                        'max' => 255,
                        'maxMessage' => 'alteris.material.name.max',
                    ]),
                ],
            ])
            ->add('material_group_id', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(
                        ['message' => 'alteris.material.material_group_id.not_blank',]
                    ),
                    new Uuid(),
                    new MaterialGroupExists(),
                    new MaterialGroupHasNotChildren()
                ],
            ])
            ->add('measure_unit_id', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(
                        ['message' => 'alteris.material.measure_unit_id.not_blank',]
                    ),
                    new Uuid(),
                    new MeasureUnitExists(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Material::class,
            'csrf_protection' => false,
        ]);
    }
}
