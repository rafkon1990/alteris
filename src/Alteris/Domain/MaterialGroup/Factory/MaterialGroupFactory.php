<?php

namespace App\Alteris\Domain\MaterialGroup\Factory;

use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;
use App\Alteris\Domain\MaterialGroup\ValueObject\Name;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use Doctrine\Common\Collections\ArrayCollection;

final class MaterialGroupFactory
{
    public function create(
        Uuid $uuid,
        Name $name,
        array $materials,
        ?MaterialGroup $parent
    ): MaterialGroup {
        return new MaterialGroup($uuid, $name, new ArrayCollection($materials), $parent);
    }
}
