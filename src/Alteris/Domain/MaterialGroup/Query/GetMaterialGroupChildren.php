<?php

namespace App\Alteris\Domain\MaterialGroup\Query;

use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;

class GetMaterialGroupChildren
{
    /** @var MaterialGroup|null */
    private $materialGroup;

    public function __construct(?MaterialGroup $materialGroup)
    {
        $this->materialGroup = $materialGroup;
    }

    public function getMaterialGroup(): ?MaterialGroup
    {
        return $this->materialGroup;
    }
}
