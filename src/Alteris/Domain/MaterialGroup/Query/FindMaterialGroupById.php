<?php

namespace App\Alteris\Domain\MaterialGroup\Query;

final class FindMaterialGroupById
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
