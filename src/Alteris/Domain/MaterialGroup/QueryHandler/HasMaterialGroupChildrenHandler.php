<?php

namespace App\Alteris\Domain\MaterialGroup\QueryHandler;

use App\Alteris\Domain\MaterialGroup\Query\HasMaterialGroupChildren;
use App\Alteris\Domain\MaterialGroup\ReadRepository\MaterialGroupRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class HasMaterialGroupChildrenHandler implements MessageHandlerInterface
{
    /** @var MaterialGroupRepositoryInterface */
    private $materialGroupRepository;

    public function __construct(MaterialGroupRepositoryInterface $materialGroupRepository)
    {
        $this->materialGroupRepository = $materialGroupRepository;
    }

    public function __invoke(HasMaterialGroupChildren $query): bool
    {
        return $this->materialGroupRepository->hasChildren($query->getId());
    }
}
