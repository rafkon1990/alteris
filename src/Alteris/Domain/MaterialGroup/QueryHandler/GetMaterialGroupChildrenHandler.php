<?php

namespace App\Alteris\Domain\MaterialGroup\QueryHandler;

use App\Alteris\Domain\MaterialGroup\Query\GetMaterialGroupChildren;
use App\Alteris\Domain\MaterialGroup\ReadRepository\MaterialGroupRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetMaterialGroupChildrenHandler implements MessageHandlerInterface
{
    /** @var MaterialGroupRepositoryInterface */
    private $materialGroupRepository;

    public function __construct(MaterialGroupRepositoryInterface $materialGroupRepository)
    {
        $this->materialGroupRepository = $materialGroupRepository;
    }

    public function __invoke(GetMaterialGroupChildren $query): array
    {
        return $this->materialGroupRepository->getChildrenHierarchy($query->getMaterialGroup());
    }
}
