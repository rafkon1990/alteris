<?php

namespace App\Alteris\Domain\MaterialGroup\QueryHandler;

use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;
use App\Alteris\Domain\MaterialGroup\Query\FindMaterialGroupRoot;
use App\Alteris\Domain\MaterialGroup\ReadRepository\MaterialGroupRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class FindMaterialGroupRootHandler implements MessageHandlerInterface
{
    /** @var MaterialGroupRepositoryInterface */
    private $materialGroupRepository;

    public function __construct(MaterialGroupRepositoryInterface $materialGroupRepository)
    {
        $this->materialGroupRepository = $materialGroupRepository;
    }

    public function __invoke(FindMaterialGroupRoot $query): ?MaterialGroup
    {
        // TODO: return read model
        return $this->materialGroupRepository->findRoot();
    }
}
