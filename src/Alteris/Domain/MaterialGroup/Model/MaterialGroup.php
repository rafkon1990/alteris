<?php

namespace App\Alteris\Domain\MaterialGroup\Model;

use App\Alteris\Domain\Material\Model\Material;
use App\Alteris\Domain\MaterialGroup\ValueObject\Name;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Tree\Traits\NestedSetEntityUuid;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="App\Alteris\Infrastructure\Persistence\Doctrine\MaterialGroupRepository")
 */
class MaterialGroup
{
    use NestedSetEntityUuid;

    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Alteris\Domain\Material\Model\Material", mappedBy="materialGroup")
     */
    private $materials;

    /**
     * @var static
     * @Gedmo\TreeParent()
     * @ORM\ManyToOne(targetEntity="MaterialGroup", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var static[]
     * @ORM\OneToMany(targetEntity="MaterialGroup", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    public function __construct(Uuid $id, Name $name, ArrayCollection $materials, ?MaterialGroup $parent)
    {
        $this->id = $id->getValue();
        $this->name = $name->getValue();
        $this->materials = $materials;
        $this->parent = $parent;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(Name $name): self
    {
        $this->name = $name->getValue();

        return $this;
    }

    /**
     * @return Collection|Material[]
     */
    public function getMaterials(): Collection
    {
        return $this->materials;
    }


    public function setParent(MaterialGroup $parent = null): void
    {
        $this->parent = $parent;
    }

    public function getParent(): ?MaterialGroup
    {
        return $this->parent;
    }
}
