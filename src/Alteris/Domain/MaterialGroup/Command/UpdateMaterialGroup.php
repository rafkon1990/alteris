<?php

namespace App\Alteris\Domain\MaterialGroup\Command;

final class UpdateMaterialGroup
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $parentId;

    public function __construct(string $id, string $name, string $parentId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->parentId = $parentId;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getParentId(): string
    {
        return $this->parentId;
    }
}
