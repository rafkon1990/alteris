<?php

namespace App\Alteris\Domain\MaterialGroup\Command;

final class MoveUpMaterialGroup
{
    /** @var string */
    private $id;

    /** @var int */
    private $positions;

    public function __construct(string $id, int $positions)
    {
        $this->id = $id;
        $this->positions = $positions;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPositions(): int
    {
        return $this->positions;
    }
}
