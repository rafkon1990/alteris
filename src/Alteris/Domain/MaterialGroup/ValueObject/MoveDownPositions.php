<?php

namespace App\Alteris\Domain\MaterialGroup\ValueObject;

use App\Alteris\Domain\Material\Exception\InvalidPositionsValueException;

class MoveDownPositions
{
    /** @var int */
    private $value;

    /**
     * @throws InvalidPositionsValueException
     */
    public function __construct(int $value)
    {
        if ($value >= 0) {
            throw new InvalidPositionsValueException($value);
        }
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
