<?php

namespace App\Alteris\Domain\MaterialGroup\ReadRepository;

use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup as Entity;

interface MaterialGroupRepositoryInterface
{
    /** @return Entity[] */
    public function findAll();

    public function findById(string $id): ?Entity;

    public function getChildrenHierarchy(?Entity $entity): array;

    public function findRoot(): ?Entity;

    public function hasChildren(string $id): bool;
}
