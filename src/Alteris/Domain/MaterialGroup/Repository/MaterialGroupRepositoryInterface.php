<?php

namespace App\Alteris\Domain\MaterialGroup\Repository;

use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup as Entity;

interface MaterialGroupRepositoryInterface
{
    public function findById(string $id): ?Entity;

    public function save(Entity... $entities): void;

    /**
     * @param int $positions positive move up, negative move down
     */
    public function moveBy(Entity $entity, int $positions): void;
}
