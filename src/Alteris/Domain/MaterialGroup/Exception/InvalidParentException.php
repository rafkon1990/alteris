<?php

namespace App\Alteris\Domain\MaterialGroup\Exception;

final class InvalidParentException extends Exception
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
        parent::__construct('Parent of material group is invalid.');
    }

    public function getId(): string
    {
        return $this->id;
    }
}
