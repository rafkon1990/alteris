<?php

namespace App\Alteris\Domain\MaterialGroup\Exception;

final class MaterialGroupNotFoundException extends Exception
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
        parent::__construct('Material group not found.');
    }

    public function getId(): string
    {
        return $this->id;
    }
}
