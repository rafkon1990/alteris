<?php

namespace App\Alteris\Domain\MaterialGroup\CommandHandler;

use App\Alteris\Domain\MaterialGroup\Command\UpdateMaterialGroup;
use App\Alteris\Domain\MaterialGroup\Exception\InvalidParentException;
use App\Alteris\Domain\MaterialGroup\Exception\MaterialGroupNotFoundException;
use App\Alteris\Domain\MaterialGroup\Exception\NameMaxException;
use App\Alteris\Domain\MaterialGroup\Exception\NameMinException;
use App\Alteris\Domain\MaterialGroup\Exception\ParentNotFoundException;
use App\Alteris\Domain\MaterialGroup\Repository\MaterialGroupRepositoryInterface;
use App\Alteris\Domain\MaterialGroup\ValueObject\Name;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateMaterialGroupHandler implements MessageHandlerInterface
{
    /** @var MaterialGroupRepositoryInterface */
    private $materialGroupRepository;

    public function __construct(MaterialGroupRepositoryInterface $materialGroupRepository)
    {
        $this->materialGroupRepository = $materialGroupRepository;
    }

    /**
     * @throws NameMaxException
     * @throws NameMinException
     * @throws MaterialGroupNotFoundException
     * @throws ParentNotFoundException
     * @throws InvalidParentException
     */
    public function __invoke(UpdateMaterialGroup $command): void
    {
        $materialGroup = $this->materialGroupRepository->findById($command->getId());
        if (null === $materialGroup) {
            throw new MaterialGroupNotFoundException($command->getId());
        }

        $parent = $this->materialGroupRepository->findById($command->getParentId());
        if ($parent === null) {
            throw new ParentNotFoundException($command->getParentId());
        }

        if ($parent->getId() === $materialGroup->getId()) {
            // can not set self as parent
            throw new InvalidParentException($parent->getId());
        }

        $materialGroup->setName(new Name($command->getName()));
        $materialGroup->setParent($parent);

        $this->materialGroupRepository->save($materialGroup);
    }
}
