<?php

namespace App\Alteris\Domain\MaterialGroup\CommandHandler;

use App\Alteris\Domain\Material\Exception\InvalidPositionsValueException;
use App\Alteris\Domain\MaterialGroup\Command\MoveDownMaterialGroup;
use App\Alteris\Domain\MaterialGroup\Exception\MaterialGroupNotFoundException;
use App\Alteris\Domain\MaterialGroup\Repository\MaterialGroupRepositoryInterface;
use App\Alteris\Domain\MaterialGroup\ValueObject\MoveDownPositions;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class MoveDownMaterialGroupHandler implements MessageHandlerInterface
{
    /** @var MaterialGroupRepositoryInterface */
    private $materialGroupRepository;

    public function __construct(MaterialGroupRepositoryInterface $materialGroupRepository)
    {
        $this->materialGroupRepository = $materialGroupRepository;
    }

    /**
     * @throws MaterialGroupNotFoundException
     * @throws InvalidPositionsValueException
     */
    public function __invoke(MoveDownMaterialGroup $command): void
    {
        $materialGroup = $this->materialGroupRepository->findById($command->getId());
        if (null === $materialGroup) {
            throw new MaterialGroupNotFoundException($command->getId());
        }

        $positions = new MoveDownPositions($command->getPositions());
        $this->materialGroupRepository->moveBy($materialGroup, $positions->getValue());
    }
}
