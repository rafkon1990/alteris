<?php

namespace App\Alteris\Domain\MaterialGroup\CommandHandler;

use App\Alteris\Domain\Material\Exception\InvalidPositionsValueException;
use App\Alteris\Domain\MaterialGroup\Command\MoveUpMaterialGroup;
use App\Alteris\Domain\MaterialGroup\Exception\MaterialGroupNotFoundException;
use App\Alteris\Domain\MaterialGroup\Repository\MaterialGroupRepositoryInterface;
use App\Alteris\Domain\MaterialGroup\ValueObject\MoveUpPositions;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class MoveUpMaterialGroupHandler implements MessageHandlerInterface
{
    /** @var MaterialGroupRepositoryInterface */
    private $materialGroupRepository;

    public function __construct(MaterialGroupRepositoryInterface $materialGroupRepository)
    {
        $this->materialGroupRepository = $materialGroupRepository;
    }

    /**
     * @throws MaterialGroupNotFoundException
     * @throws InvalidPositionsValueException
     */
    public function __invoke(MoveUpMaterialGroup $command): void
    {
        $materialGroup = $this->materialGroupRepository->findById($command->getId());
        if (null === $materialGroup) {
            throw new MaterialGroupNotFoundException($command->getId());
        }

        $positions = new MoveUpPositions($command->getPositions());
        $this->materialGroupRepository->moveBy($materialGroup, $positions->getValue());
    }
}
