<?php

namespace App\Alteris\Domain\MaterialGroup\CommandHandler;

use App\Alteris\Domain\MaterialGroup\Factory\MaterialGroupFactory;
use App\Alteris\Domain\MaterialGroup\Command\CreateMaterialGroup;
use App\Alteris\Domain\MaterialGroup\Exception\IdNotUniqueException;
use App\Alteris\Domain\MaterialGroup\Exception\NameMaxException;
use App\Alteris\Domain\MaterialGroup\Exception\NameMinException;
use App\Alteris\Domain\MaterialGroup\Exception\ParentNotFoundException;
use App\Alteris\Domain\MaterialGroup\Repository\MaterialGroupRepositoryInterface;
use App\Alteris\Domain\MaterialGroup\ValueObject\Name;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateMaterialGroupHandler implements MessageHandlerInterface
{
    /** @var MaterialGroupRepositoryInterface */
    private $materialGroupRepository;

    /** @var MaterialGroupFactory */
    private $materialGroupFactory;

    public function __construct(
        MaterialGroupRepositoryInterface $materialGroupRepository,
        MaterialGroupFactory $materialGroupFactory
    ) {
        $this->materialGroupRepository = $materialGroupRepository;
        $this->materialGroupFactory = $materialGroupFactory;
    }

    /**
     * @throws ParentNotFoundException
     * @throws NameMaxException
     * @throws NameMinException
     * @throws IdNotUniqueException
     */
    public function __invoke(CreateMaterialGroup $command): void
    {
        $uuid = new Uuid($command->getId());
        if ($this->materialGroupRepository->findById($uuid->getValue())) {
            throw new IdNotUniqueException($uuid->getValue());
        }

        $parent = $this->materialGroupRepository->findById($command->getParentId());
        if ($parent === null) {
            throw new ParentNotFoundException($command->getParentId());
        }

        $entity = $this->materialGroupFactory->create($uuid, new Name($command->getName()), [], $parent);
        $this->materialGroupRepository->save($entity);
    }
}
