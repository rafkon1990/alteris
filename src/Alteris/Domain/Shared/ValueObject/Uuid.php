<?php

namespace App\Alteris\Domain\Shared\ValueObject;

class Uuid
{
    /** @var string */
    private $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
