<?php

namespace App\Alteris\Domain\Material\Model;

use App\Alteris\Domain\Material\ValueObject\Code;
use App\Alteris\Domain\Material\ValueObject\Name;
use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;
use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Alteris\Infrastructure\Persistence\Doctrine\MaterialRepository")
 */
class Material
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var MaterialGroup
     * @ORM\ManyToOne(targetEntity="App\Alteris\Domain\MaterialGroup\Model\MaterialGroup", inversedBy="materials")
     * @ORM\JoinColumn(nullable=false)
     */
    private $materialGroup;

    /**
     * @var MeasureUnit
     * @ORM\ManyToOne(targetEntity="App\Alteris\Domain\MeasureUnit\Model\MeasureUnit")
     * @ORM\JoinColumn(nullable=false)
     */
    private $measureUnit;

    public function __construct(
        Uuid $id,
        Code $code,
        Name $name,
        MaterialGroup $materialGroup,
        MeasureUnit $measureUnit
    ) {
        $this->id = $id->getValue();
        $this->code = $code->getValue();
        $this->name = $name->getValue();
        $this->materialGroup = $materialGroup;
        $this->measureUnit = $measureUnit;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(Code $code): self
    {
        $this->code = $code->getValue();

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(Name $name): self
    {
        $this->name = $name->getValue();

        return $this;
    }

    public function getMaterialGroup(): ?MaterialGroup
    {
        return $this->materialGroup;
    }

    public function setMaterialGroup(?MaterialGroup $materialGroup): self
    {
        $this->materialGroup = $materialGroup;

        return $this;
    }

    public function getMeasureUnit(): ?MeasureUnit
    {
        return $this->measureUnit;
    }

    public function setMeasureUnit(?MeasureUnit $measureUnit): self
    {
        $this->measureUnit = $measureUnit;

        return $this;
    }
}
