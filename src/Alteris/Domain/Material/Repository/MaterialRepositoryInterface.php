<?php

namespace App\Alteris\Domain\Material\Repository;

use App\Alteris\Domain\Material\Model\Material as Entity;

interface MaterialRepositoryInterface
{
    public function findById(string $id): ?Entity;

    /** @return Entity[] */
    public function findAll();

    public function save(Entity $entity): void;
}
