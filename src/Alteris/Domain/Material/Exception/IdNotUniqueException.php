<?php

namespace App\Alteris\Domain\Material\Exception;

class IdNotUniqueException extends Exception
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
        parent::__construct('Id not found.');
    }

    public function getId(): string
    {
        return $this->id;
    }
}
