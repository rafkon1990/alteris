<?php

namespace App\Alteris\Domain\Material\Exception;

final class MaterialNotFoundException extends Exception
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
        parent::__construct('Material not found.');
    }

    public function getId(): string
    {
        return $this->id;
    }
}
