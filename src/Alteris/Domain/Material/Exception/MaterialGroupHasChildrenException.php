<?php

namespace App\Alteris\Domain\Material\Exception;

final class MaterialGroupHasChildrenException extends Exception
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
        parent::__construct('Material group has children.');
    }

    public function getId(): string
    {
        return $this->id;
    }
}
