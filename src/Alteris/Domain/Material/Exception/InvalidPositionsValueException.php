<?php

namespace App\Alteris\Domain\Material\Exception;

class InvalidPositionsValueException extends Exception
{
    /** @var int */
    private $value;

    public function __construct(int $value)
    {
        $this->value = $value;
        parent::__construct('Invalid positions value.');
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
