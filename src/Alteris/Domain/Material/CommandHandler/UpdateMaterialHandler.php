<?php

namespace App\Alteris\Domain\Material\CommandHandler;

use App\Alteris\Domain\Material\Command\UpdateMaterial;
use App\Alteris\Domain\Material\Exception\CodeMaxException;
use App\Alteris\Domain\Material\Exception\CodeMinException;
use App\Alteris\Domain\Material\Exception\MaterialGroupHasChildrenException;
use App\Alteris\Domain\Material\Exception\MaterialNotFoundException;
use App\Alteris\Domain\Material\Exception\NameMaxException;
use App\Alteris\Domain\Material\Exception\NameMinException;
use App\Alteris\Domain\Material\Repository\MaterialRepositoryInterface;
use App\Alteris\Domain\Material\ValueObject\Code;
use App\Alteris\Domain\Material\ValueObject\Name;
use App\Alteris\Domain\MaterialGroup\Exception\MaterialGroupNotFoundException;
use App\Alteris\Domain\MaterialGroup\ReadRepository\MaterialGroupRepositoryInterface;
use App\Alteris\Domain\MeasureUnit\Exception\MeasureUnitNotFoundException;
use App\Alteris\Domain\MeasureUnit\Repository\MeasureUnitRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateMaterialHandler implements MessageHandlerInterface
{
    /** @var MaterialRepositoryInterface */
    private $materialRepository;

    /** @var MaterialGroupRepositoryInterface */
    private $materialGroupRepository;

    /** @var MeasureUnitRepositoryInterface */
    private $measureUnitRepository;

    public function __construct(
        MaterialRepositoryInterface $materialRepository,
        MaterialGroupRepositoryInterface $materialGroupRepository,
        MeasureUnitRepositoryInterface $measureUnitRepository
    ) {
        $this->materialRepository = $materialRepository;
        $this->materialGroupRepository = $materialGroupRepository;
        $this->measureUnitRepository = $measureUnitRepository;
    }

    /**
     * @param UpdateMaterial $command
     * @throws MaterialGroupHasChildrenException
     * @throws MaterialNotFoundException
     * @throws MeasureUnitNotFoundException
     * @throws CodeMaxException
     * @throws CodeMinException
     * @throws NameMaxException
     * @throws NameMinException
     * @throws MaterialGroupNotFoundException
     */
    public function __invoke(UpdateMaterial $command): void
    {
        $material = $this->materialRepository->findById($command->getId());
        if ($material === null) {
            throw new MaterialNotFoundException($command->getId());
        }

        $materialGroup = $this->materialGroupRepository->find($command->getMaterialGroupId());
        if ($materialGroup === null) {
            throw new MaterialGroupNotFoundException($command->getMaterialGroupId());
        }

        if ($this->materialGroupRepository->hasChildren($command->getMaterialGroupId())) {
            throw new MaterialGroupHasChildrenException($command->getMaterialGroupId());
        }

        $measureUnit = $this->measureUnitRepository->find($command->getMeasureUnitId());
        if ($measureUnit === null) {
            throw new MeasureUnitNotFoundException($command->getMeasureUnitId());
        }

        $material
            ->setMeasureUnit($measureUnit)
            ->setMaterialGroup($materialGroup)
            ->setCode(new Code($command->getCode()))
            ->setName(new Name($command->getName()));

        $this->materialRepository->save($material);
    }
}
