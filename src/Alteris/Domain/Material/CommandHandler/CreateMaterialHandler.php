<?php

namespace App\Alteris\Domain\Material\CommandHandler;

use App\Alteris\Domain\Material\Command\CreateMaterial;
use App\Alteris\Domain\Material\Exception\CodeMaxException;
use App\Alteris\Domain\Material\Exception\CodeMinException;
use App\Alteris\Domain\Material\Exception\NameMaxException;
use App\Alteris\Domain\Material\Exception\NameMinException;
use App\Alteris\Domain\Material\Factory\MaterialFactory;
use App\Alteris\Domain\Material\Repository\MaterialRepositoryInterface;
use App\Alteris\Domain\Material\ValueObject\Code;
use App\Alteris\Domain\Material\ValueObject\Name;
use App\Alteris\Domain\Material\Exception\IdNotUniqueException;
use App\Alteris\Domain\MaterialGroup\Exception\MaterialGroupNotFoundException;
use App\Alteris\Domain\MaterialGroup\Repository\MaterialGroupRepositoryInterface;
use App\Alteris\Domain\MeasureUnit\Exception\MeasureUnitNotFoundException;
use App\Alteris\Domain\MeasureUnit\Repository\MeasureUnitRepositoryInterface;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateMaterialHandler implements MessageHandlerInterface
{
    /** @var MaterialRepositoryInterface */
    private $materialRepository;

    /** @var MaterialGroupRepositoryInterface */
    private $materialGroupRepository;

    /** @var MeasureUnitRepositoryInterface */
    private $measureUnitRepository;

    /** @var MaterialFactory */
    private $materialFactory;

    public function __construct(
        MaterialRepositoryInterface $materialRepository,
        MaterialGroupRepositoryInterface $materialGroupRepository,
        MeasureUnitRepositoryInterface $measureUnitRepository,
        MaterialFactory $materialFactory
    ) {
        $this->materialRepository = $materialRepository;
        $this->materialFactory = $materialFactory;
        $this->materialGroupRepository = $materialGroupRepository;
        $this->measureUnitRepository = $measureUnitRepository;
    }

    /**
     * @throws MeasureUnitNotFoundException
     * @throws CodeMaxException
     * @throws CodeMinException
     * @throws NameMaxException
     * @throws NameMinException
     * @throws MaterialGroupNotFoundException
     * @throws IdNotUniqueException
     */
    public function __invoke(CreateMaterial $command): void
    {
        $uuid = new Uuid($command->getId());
        if ($this->materialGroupRepository->findById($uuid->getValue())) {
            throw new IdNotUniqueException($uuid->getValue());
        }

        $materialGroup = $this->materialGroupRepository->find($command->getMaterialGroupId());
        if ($materialGroup === null) {
            throw new MaterialGroupNotFoundException($command->getMaterialGroupId());
        }

        $measureUnit = $this->measureUnitRepository->find($command->getMeasureUnitId());
        if ($measureUnit === null) {
            throw new MeasureUnitNotFoundException($command->getMeasureUnitId());
        }

        $material = $this->materialFactory->create(
            $uuid,
            new Code($command->getCode()),
            new Name($command->getName()),
            $materialGroup,
            $measureUnit
        );
        $this->materialRepository->save($material);
    }
}
