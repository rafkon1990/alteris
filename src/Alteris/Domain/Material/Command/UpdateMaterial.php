<?php

namespace App\Alteris\Domain\Material\Command;

final class UpdateMaterial
{
    /** @var string */
    private $id;

    /** @var string */
    private $code;

    /** @var string */
    private $name;

    /** @var string */
    private $measureUnitId;

    /** @var string */
    private $materialGroupId;

    public function __construct(
        string $id,
        string $code,
        string $name,
        string $measureUnitId,
        string $materialGroupId
    ) {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
        $this->measureUnitId = $measureUnitId;
        $this->materialGroupId = $materialGroupId;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMeasureUnitId(): string
    {
        return $this->measureUnitId;
    }

    public function getMaterialGroupId(): string
    {
        return $this->materialGroupId;
    }
}
