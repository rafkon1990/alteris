<?php

namespace App\Alteris\Domain\Material\Query;

final class FindMaterialById
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
