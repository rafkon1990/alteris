<?php

namespace App\Alteris\Domain\Material\ValueObject;

use App\Alteris\Domain\Material\Exception\CodeMaxException;
use App\Alteris\Domain\Material\Exception\CodeMinException;

final class Code
{
    /** @var string */
    private $value;

    /**
     * @throws CodeMaxException
     * @throws CodeMinException
     */
    public function __construct(string $value)
    {
        if (strlen($value) < 1) {
            throw new CodeMinException();
        }
        if (strlen($value) > 32) {
            throw new CodeMaxException();
        }
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
