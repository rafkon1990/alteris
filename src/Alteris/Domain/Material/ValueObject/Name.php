<?php

namespace App\Alteris\Domain\Material\ValueObject;

use App\Alteris\Domain\Material\Exception\NameMaxException;
use App\Alteris\Domain\Material\Exception\NameMinException;

final class Name
{
    /** @var string */
    private $value;

    /**
     * @throws NameMinException
     * @throws NameMaxException
     */
    public function __construct(string $value)
    {
        if (strlen($value) < 3) {
            throw new NameMinException();
        }
        if (strlen($value) > 255) {
            throw new NameMaxException();
        }
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
