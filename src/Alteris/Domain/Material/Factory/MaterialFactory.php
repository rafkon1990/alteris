<?php

namespace App\Alteris\Domain\Material\Factory;

use App\Alteris\Domain\Material\Model\Material as Entity;
use App\Alteris\Domain\Material\ValueObject\Code;
use App\Alteris\Domain\Material\ValueObject\Name;
use App\Alteris\Domain\MaterialGroup\Model\MaterialGroup;
use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit;
use App\Alteris\Domain\Shared\ValueObject\Uuid;

final class MaterialFactory
{
    public function create(
        Uuid $id,
        Code $code,
        Name $name,
        MaterialGroup $materialGroup,
        MeasureUnit $measureUnit
    ): Entity {
        return new Entity($id, $code, $name, $materialGroup, $measureUnit);
    }
}
