<?php

namespace App\Alteris\Domain\Material\QueryHandler;

use App\Alteris\Domain\Material\Query\FindAllMaterials;
use App\Alteris\Infrastructure\Persistence\Doctrine\MaterialRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class FindAllMaterialsHandler implements MessageHandlerInterface
{
    /** @var MaterialRepository */
    private $repository;

    public function __construct(MaterialRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(FindAllMaterials $query): array
    {
        return $this->repository->findAll();
    }
}
