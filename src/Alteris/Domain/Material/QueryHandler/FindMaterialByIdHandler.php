<?php

namespace App\Alteris\Domain\Material\QueryHandler;

use App\Alteris\Domain\Material\Model\Material;
use App\Alteris\Domain\Material\Query\FindMaterialById;
use App\Alteris\Infrastructure\Persistence\Doctrine\MaterialRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class FindMaterialByIdHandler implements MessageHandlerInterface
{
    /** @var MaterialRepository */
    private $materialRepository;

    public function __construct(MaterialRepository $materialRepository)
    {
        $this->materialRepository = $materialRepository;
    }

    public function __invoke(FindMaterialById $query): ?Material
    {
        return $this->materialRepository->findById($query->getId());
    }
}
