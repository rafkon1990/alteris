<?php

namespace App\Alteris\Domain\MeasureUnit\CommandHandler;

use App\Alteris\Domain\MeasureUnit\Command\UpdateMeasureUnit;
use App\Alteris\Domain\MeasureUnit\Exception\MeasureUnitNotFoundException;
use App\Alteris\Domain\MeasureUnit\Exception\NameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\NameMinException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMinException;
use App\Alteris\Domain\MeasureUnit\Repository\MeasureUnitRepositoryInterface;
use App\Alteris\Domain\MeasureUnit\ValueObject\Name;
use App\Alteris\Domain\MeasureUnit\ValueObject\ShortName;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateMeasureUnitHandler implements MessageHandlerInterface
{
    /** @var MeasureUnitRepositoryInterface */
    private $measureUnitRepository;

    public function __construct(MeasureUnitRepositoryInterface $measureUnitRepository)
    {
        $this->measureUnitRepository = $measureUnitRepository;
    }

    /**
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ShortNameMaxException
     * @throws ShortNameMinException
     * @throws MeasureUnitNotFoundException
     */
    public function __invoke(UpdateMeasureUnit $command): void
    {
        $measureUnit = $this->measureUnitRepository->findById($command->getId());
        if (null === $measureUnit) {
            throw new MeasureUnitNotFoundException($command->getId());
        }

        $measureUnit->setShortName(new ShortName($command->getShortName()));
        $measureUnit->setName(new Name($command->getName()));

        $this->measureUnitRepository->save($measureUnit);
    }
}
