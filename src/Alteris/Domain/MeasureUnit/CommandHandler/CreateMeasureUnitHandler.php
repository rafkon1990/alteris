<?php

namespace App\Alteris\Domain\MeasureUnit\CommandHandler;

use App\Alteris\Domain\MeasureUnit\Command\CreateMeasureUnit;
use App\Alteris\Domain\MeasureUnit\Exception\IdNotUniqueException;
use App\Alteris\Domain\MeasureUnit\Exception\NameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\NameMinException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMinException;
use App\Alteris\Domain\MeasureUnit\Factory\MeasureUnitFactory;
use App\Alteris\Domain\MeasureUnit\Repository\MeasureUnitRepositoryInterface;
use App\Alteris\Domain\MeasureUnit\ValueObject\Name;
use App\Alteris\Domain\MeasureUnit\ValueObject\ShortName;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateMeasureUnitHandler implements MessageHandlerInterface
{
    /** @var MeasureUnitRepositoryInterface */
    private $measureUnitRepository;

    /** @var MeasureUnitFactory */
    private $measureUnitFactory;

    public function __construct(
        MeasureUnitRepositoryInterface $measureUnitRepository,
        MeasureUnitFactory $measureUnitFactory
    ) {
        $this->measureUnitRepository = $measureUnitRepository;
        $this->measureUnitFactory = $measureUnitFactory;
    }

    /**
     * @throws NameMaxException
     * @throws NameMinException
     * @throws ShortNameMaxException
     * @throws ShortNameMinException
     * @throws IdNotUniqueException
     */
    public function __invoke(CreateMeasureUnit $command): void
    {
        $uuid = new Uuid($command->getId());
        if ($this->measureUnitRepository->findById($uuid->getValue())) {
            throw new IdNotUniqueException($uuid->getValue());
        }

        $entity = $this->measureUnitFactory->create(
            $uuid,
            new Name($command->getName()),
            new ShortName($command->getShortName())
        );
        $this->measureUnitRepository->save($entity);
    }
}
