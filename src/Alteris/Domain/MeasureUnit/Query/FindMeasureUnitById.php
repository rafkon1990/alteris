<?php

namespace App\Alteris\Domain\MeasureUnit\Query;

final class FindMeasureUnitById
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
