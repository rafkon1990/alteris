<?php

namespace App\Alteris\Domain\MeasureUnit\Factory;

use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit as Entity;
use App\Alteris\Domain\MeasureUnit\ValueObject\Name;
use App\Alteris\Domain\MeasureUnit\ValueObject\ShortName;
use App\Alteris\Domain\Shared\ValueObject\Uuid;

final class MeasureUnitFactory
{
    public function create(
        Uuid $id,
        Name $name,
        ShortName $shortName
    ): Entity {
        return new Entity($id, $name, $shortName);
    }
}
