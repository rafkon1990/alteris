<?php

namespace App\Alteris\Domain\MeasureUnit\Repository;

use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit as Entity;

interface MeasureUnitRepositoryInterface
{
    public function findById(string $id): ?Entity;

    /** @return Entity[] */
    public function findAll();

    public function save(Entity $entity): void;
}
