<?php

namespace App\Alteris\Domain\MeasureUnit\Model;

use App\Alteris\Domain\MeasureUnit\ValueObject\Name;
use App\Alteris\Domain\MeasureUnit\ValueObject\ShortName;
use App\Alteris\Domain\Shared\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Alteris\Infrastructure\Persistence\Doctrine\MeasureUnitRepository")
 */
class MeasureUnit
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", name="short_name")
     */
    private $shortName;

    public function __construct(Uuid $id, Name $name, ShortName $shortName)
    {
        $this->id = $id->getValue();
        $this->name = $name->getValue();
        $this->shortName = $shortName->getValue();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(Name $name): self
    {
        $this->name = $name->getValue();

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(ShortName $shortName): self
    {
        $this->shortName = $shortName->getValue();

        return $this;
    }
}
