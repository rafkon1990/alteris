<?php

namespace App\Alteris\Domain\MeasureUnit\QueryHandler;

use App\Alteris\Domain\MeasureUnit\Query\FindAllMeasureUnits;
use App\Alteris\Domain\MeasureUnit\Repository\MeasureUnitRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class FindAllMeasureUnitsHandler implements MessageHandlerInterface
{
    /** @var MeasureUnitRepositoryInterface */
    private $repository;

    public function __construct(MeasureUnitRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(FindAllMeasureUnits $query): array
    {
        // TODO: return read models
        return $this->repository->findAll();
    }
}
