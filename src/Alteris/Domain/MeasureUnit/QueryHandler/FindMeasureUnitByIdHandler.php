<?php

namespace App\Alteris\Domain\MeasureUnit\QueryHandler;

use App\Alteris\Domain\MeasureUnit\Model\MeasureUnit;
use App\Alteris\Domain\MeasureUnit\Query\FindMeasureUnitById;
use App\Alteris\Domain\MeasureUnit\Repository\MeasureUnitRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class FindMeasureUnitByIdHandler implements MessageHandlerInterface
{
    /** @var MeasureUnitRepositoryInterface */
    private $measureUnitRepository;

    public function __construct(MeasureUnitRepositoryInterface $measureUnitRepository)
    {
        $this->measureUnitRepository = $measureUnitRepository;
    }

    public function __invoke(FindMeasureUnitById $query): ?MeasureUnit
    {
        // TODO: return read models
        return $this->measureUnitRepository->findById($query->getId());
    }
}
