<?php

namespace App\Alteris\Domain\MeasureUnit\ValueObject;

use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\ShortNameMinException;

final class ShortName
{
    /** @var string */
    private $value;

    /**
     * @throws ShortNameMaxException
     * @throws ShortNameMinException
     */
    public function __construct(string $value)
    {
        if (strlen($value) < 1) {
            throw new ShortNameMinException();
        }
        if (strlen($value) > 5) {
            throw new ShortNameMaxException();
        }
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
