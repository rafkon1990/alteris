<?php

namespace App\Alteris\Domain\MeasureUnit\ValueObject;

use App\Alteris\Domain\MeasureUnit\Exception\NameMaxException;
use App\Alteris\Domain\MeasureUnit\Exception\NameMinException;

final class Name
{
    /** @var string */
    private $value;

    /**
     * @throws NameMinException
     * @throws NameMaxException
     */
    public function __construct(string $value)
    {
        if (strlen($value) < 3) {
            throw new NameMinException();
        }
        if (strlen($value) > 255) {
            throw new NameMaxException();
        }
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
