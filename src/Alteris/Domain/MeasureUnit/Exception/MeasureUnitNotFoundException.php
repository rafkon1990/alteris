<?php

namespace App\Alteris\Domain\MeasureUnit\Exception;

final class MeasureUnitNotFoundException extends Exception
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
        parent::__construct('Measure unit not found.');
    }

    public function getId(): string
    {
        return $this->id;
    }
}
