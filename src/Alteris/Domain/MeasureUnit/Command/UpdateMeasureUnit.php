<?php

namespace App\Alteris\Domain\MeasureUnit\Command;

final class UpdateMeasureUnit
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $shortName;

    public function __construct(string $id, string $name, string $shortName)
    {
        $this->id = $id;
        $this->name = $name;
        $this->shortName = $shortName;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }
}
