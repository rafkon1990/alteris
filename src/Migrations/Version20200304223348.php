<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200304223348 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE material (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', material_group_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', measure_unit_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_7CBE7595B88CFDE5 (material_group_id), INDEX IDX_7CBE759563C6A475 (measure_unit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE material_group (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', parent_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) NOT NULL, root VARCHAR(255) DEFAULT NULL, lvl INT NOT NULL, lft INT NOT NULL, rgt INT NOT NULL, INDEX IDX_3B27E88B727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE measure_unit (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) NOT NULL, short_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE material ADD CONSTRAINT FK_7CBE7595B88CFDE5 FOREIGN KEY (material_group_id) REFERENCES material_group (id)');
        $this->addSql('ALTER TABLE material ADD CONSTRAINT FK_7CBE759563C6A475 FOREIGN KEY (measure_unit_id) REFERENCES measure_unit (id)');
        $this->addSql('ALTER TABLE material_group ADD CONSTRAINT FK_3B27E88B727ACA70 FOREIGN KEY (parent_id) REFERENCES material_group (id) ON DELETE CASCADE');
        $this->addSql('INSERT INTO symfony.material_group (id, parent_id, name, root, lvl, lft, rgt) VALUES (\'02e9f56f-8468-47c7-8a0c-d50e76105892\', null, \'root\', null, 0, 0, 0);');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material DROP FOREIGN KEY FK_7CBE7595B88CFDE5');
        $this->addSql('ALTER TABLE material_group DROP FOREIGN KEY FK_3B27E88B727ACA70');
        $this->addSql('ALTER TABLE material DROP FOREIGN KEY FK_7CBE759563C6A475');
        $this->addSql('DROP TABLE material');
        $this->addSql('DROP TABLE material_group');
        $this->addSql('DROP TABLE measure_unit');
    }
}
